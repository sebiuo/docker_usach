# se usa "docker-compose exec web python manage.py COMANDO" para ejecutar los comando de abajo
# docker-compose exec db psql --username=pingeso --dbname=tiendita   -> para loguear a la base de datos
from flask.cli import FlaskGroup

from project import app, db, User


cli = FlaskGroup(app)


@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()

# ojo, que en archivo __init__.py se debe espicificar cual será la entrada
@cli.command("seed_db")
def seed_db():
    db.session.add(User(email="sebastian.villalobos.p@usach.cl", nombre="Sebastian Villalobos"))
    db.session.commit()


if __name__ == "__main__":
    cli()

